﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour {

	public List<GameObject> chessPrefabs;
	public ChessPieces[,] Chessmans{ set; get; }
	private ChessPieces chessSelected;
	private List<GameObject> activeChess = new List<GameObject>();

	private const float tileSize = 1.0f;
	private const float tileOffset = 0.5f;
	private int selectionX = -1;
	private int selectionY = -1;
	public bool isWhiteTurn = true;


	private void DrawBoard (){
		Vector3 widthLine = Vector3.right * 8;
		Vector3 heightLine = Vector3.forward * 8;

		for (int i = 0; i <= 8; i++) {
			Vector3 start = Vector3.forward * i;
			Debug.DrawLine (start, start+widthLine);
			for (int j= 0; j <= 8; j++) {
				Vector3 start2 = Vector3.right * j;
				Debug.DrawLine (start2, start2+heightLine);
			}
		}
		if (selectionX >= 0 && selectionY >= 0) {
			Debug.DrawLine (Vector3.forward * selectionY + Vector3.right * selectionX,
				Vector3.forward * (selectionY + 1) + Vector3.right * (selectionX + 1));
			Debug.DrawLine (Vector3.forward * (selectionY + 1) + Vector3.right * selectionX,
				Vector3.forward * selectionY + Vector3.right * (selectionX + 1));
		}
	}
	private void SpawnPieces(int index, int x, int y){
		GameObject go = Instantiate (chessPrefabs [index], GetTileCentre(x,y), Quaternion.identity) as GameObject;
		go.transform.SetParent (transform);
		Chessmans [x, y] = go.GetComponent<ChessPieces> ();
		Chessmans [x, y].SetPosition (x, y);
		activeChess.Add (go);

	}
	private void UpdateSelection(){
		if (!Camera.main)
			return;
		RaycastHit hit;
		if (Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit, 25.0f, LayerMask.GetMask ("ChessPlane"))) {
			selectionX = (int)hit.point.x;
			selectionY = (int)hit.point.z;
			Debug.Log (hit.point + " you twat");
		} else {
			selectionX = -1;
			selectionY = -1;
		}
	}
	// Use this for initialization
	void Start () {
		activeChess = new List<GameObject>();
		Chessmans = new ChessPieces[8, 8];
		SpawnPieces(0, 3, 7);
		SpawnPieces (1, 4, 7);
		SpawnPieces (2, 0, 7);
		SpawnPieces (2, 7, 7);
		SpawnPieces (3, 5, 7);
		SpawnPieces (3, 2, 7);
		SpawnPieces (4, 1, 7);
		SpawnPieces (4, 6, 7);
		for (int i = 0; i < 8; i++) {
			SpawnPieces (5, i, 6);
		}

		SpawnPieces(6, 3, 0);
		SpawnPieces (7, 4, 0);
		SpawnPieces (8, 0, 0);
		SpawnPieces (8, 7, 0);
		SpawnPieces (9, 5, 0);
		SpawnPieces (9, 2, 0);
		SpawnPieces (10, 1, 0);
		SpawnPieces (10, 6, 0);
		for (int i = 0; i < 8; i++) {
			SpawnPieces (11, i, 1);
		}
	}
	private Vector3 GetTileCentre(int x, int y){
		Vector3 origin = Vector3.zero;
		origin.x += (tileSize * x) + tileOffset;
		origin.z = +(tileSize * y) + tileOffset;
		return origin;
	}

	// Update is called once per frame
	void Update () {
		UpdateSelection ();
		DrawBoard ();
		if (Input.GetMouseButtonDown (0)) {
			if (selectionX >= 0 && selectionY >= 0) {
				if (chessSelected == null) {
					//Select chess piece
					SelectChessPiece(selectionX,selectionY);
				} else {
					//Move piece
					MoveChessPiece(selectionX,selectionY);
				}
			}
		}

	}
	private void SelectChessPiece(int x, int y){
		if (Chessmans [x, y] == null) {
			return;
		}
		if (Chessmans[x,y].isWhite != isWhiteTurn){
			return;
		}
			chessSelected = Chessmans[x,y];
	}
	private void MoveChessPiece(int x, int y){
				if (chessSelected.PossibleMove(x,y)){
			ChessPieces c = Chessmans [x, y];
			if (c != null && c.isWhite != isWhiteTurn){
				activeChess.Remove(c.gameObject);
				Destroy(c.gameObject);
			}
					
					Chessmans[chessSelected.CurrentX,chessSelected.CurrentY] = null;
					chessSelected.transform.position = GetTileCentre(x,y);
					Chessmans[x,y] = chessSelected;
					isWhiteTurn = !isWhiteTurn;
				}
				chessSelected = null;
	}
}


