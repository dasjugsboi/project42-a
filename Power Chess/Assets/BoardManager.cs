﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : MonoBehaviour {

	public List<GameObject> chessPrefabs;
	private List<GameObject> activeChess = new List<GameObject>();

	private const float tileSize = 1.0f;
	private const float tileOffset = 0.5f;
	private int selectionX = -1;
	private int selectionY = -1;

	private void DrawBoard (){
		Vector3 widthLine = Vector3.right * 8;
		Vector3 heightLine = Vector3.forward * 8;

		for (int i = 0; i <= 8; i++) {
			Vector3 start = Vector3.forward * i;
			Debug.DrawLine (start, start+widthLine);
			for (int j= 0; j <= 8; j++) {
				Vector3 start2 = Vector3.right * j;
				Debug.DrawLine (start2, start2+heightLine);
			}
		}
		if (selectionX >= 0 && selectionY >= 0) {
			Debug.DrawLine (Vector3.forward * selectionY + Vector3.right * selectionX,
				Vector3.forward * (selectionY + 1) + Vector3.right * (selectionX + 1));
			Debug.DrawLine (Vector3.forward * (selectionY + 1) + Vector3.right * selectionX,
				Vector3.forward * selectionY + Vector3.right * (selectionX + 1));
		}
	}
	private void SpawnPieces(int index, Vector3 position){
		GameObject go = Instantiate (chessPrefabs [index], position, Quaternion.identity) as GameObject;
		go.transform.SetParent (transform);
		activeChess.Add (go);

	}
	private void UpdateSelection(){
		if (!Camera.main)
			return;
		RaycastHit hit;
		if (Physics.Raycast (Camera.main.ScreenPointToRay (Input.mousePosition), out hit, 25.0f, LayerMask.GetMask ("ChessPlane"))) {
			selectionX = (int)hit.point.x;
			selectionY = (int)hit.point.z;
			Debug.Log (hit.point + " you twat");
		} else {
			selectionX = -1;
			selectionY = -1;
		}
	}
	// Use this for initialization
	void Start () {
		SpawnPieces(0, GetTileCentre(3, 7));
		SpawnPieces (1, GetTileCentre (4, 7));
		SpawnPieces (2, GetTileCentre (0, 7));
		SpawnPieces (2, GetTileCentre (7, 7));
		SpawnPieces (3, GetTileCentre (5, 7));
		SpawnPieces (3, GetTileCentre (2, 7));
		SpawnPieces (4, GetTileCentre (1, 7));
		SpawnPieces (4, GetTileCentre (6, 7));
		for (int i = 0; i < 8; i++) {
			SpawnPieces (5, GetTileCentre (i, 6));
		}

		SpawnPieces(6, GetTileCentre(3, 0));
		SpawnPieces (7, GetTileCentre (4, 0));
		SpawnPieces (8, GetTileCentre (0, 0));
		SpawnPieces (8, GetTileCentre (7, 0));
		SpawnPieces (9, GetTileCentre (5, 0));
		SpawnPieces (9, GetTileCentre (2, 0));
		SpawnPieces (10, GetTileCentre (1, 0));
		SpawnPieces (10, GetTileCentre (6, 0));
		for (int i = 0; i < 8; i++) {
			SpawnPieces (11, GetTileCentre (i, 1));
		}
	}
	private Vector3 GetTileCentre(int x, int y){
		Vector3 origin = Vector3.zero;
		origin.x += (tileSize * x) + tileOffset;
		origin.z = +(tileSize * y) + tileOffset;
		return origin;
	}

	// Update is called once per frame
	void Update () {
		UpdateSelection ();
		DrawBoard ();

	}
}


